var express = require('express')
var app = express()

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

app.get('/', function(req,res){
    var number = Math.floor((Math.random() * 6) + 1)
    res.type('json')
    res.send({number: number})
})

app.listen(3000, function(){
    console.log("Random is running, let's play a game")
})
