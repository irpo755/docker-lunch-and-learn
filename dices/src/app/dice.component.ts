import { Component } from '@angular/core';
import { RandomService } from './random.service';

@Component({
  selector: 'dice',
  templateUrl: './dice.template.html',
  styleUrls: ['./dice.style.css']
})

export class DiceComponent {
  
  rotate = this.getNumberRotateStyle(1);

  wait = false;

  buttonDisabled = false;

  actualNumber;

  constructor(private randomService: RandomService) { }

  roll(num = 2): void{
    if(!this.buttonDisabled) this.buttonDisabled = true;
    var number = num;
    this.wait = true;
    
    this.waiting(number);

    setTimeout(() => {
      this.randomService.getNumber().then(
        response => {
          var random = JSON.parse(response._body);
          this.buttonDisabled = false;
          this.wait = false;
          this.rotateDice(random.number);
          this.actualNumber = random.number;
        }
      );
    },3000);    
  }

  waiting(num: number): void{
      setTimeout(() => {
          if(this.wait){
          this.rotateDice(num);
          num = num + 1;
          if(num == 7) num = 1;  
          this.waiting(num);
      }   
      },400);
    
  }

  rotateDice(num: number): void{
    this.rotate = this.getNumberRotateStyle(num);
  }
  
  getNumberRotateStyle(num: number): string{
    switch(num){
      case 1:
       return "rotateX(0deg)";
      case 2:
        return "rotateX(90deg)";
      case 3:
        return "rotateY(-90deg)";
      case 4:
        return "rotateY(90deg)";
      case 5:
        return "rotateX(-90deg)";
      case 6:
        return "rotateY(180deg)";
      default:
        return "rotateX(0deg)";
    }
  }
}
