import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { DiceComponent } from './dice.component';
import { RandomService } from './random.service';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    DiceComponent
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [RandomService],
  bootstrap: [DiceComponent]
})
export class AppModule { }
