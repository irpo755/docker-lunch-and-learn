import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

@Injectable()
export class RandomService{
    constructor (
        private http: Http
    ) {}

    getNumber(): Promise<any> {
        return this.http.get('http://localhost:3000').toPromise();
    }
}